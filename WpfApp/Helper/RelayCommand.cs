﻿using System;
using System.Windows.Input;

namespace WpfApp.Helper
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Input.ICommand" />
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// The execute
        /// </summary>
        readonly Action<object> _execute;
        /// <summary>
        /// The can execute
        /// </summary>
        readonly Predicate<object> _canExecute;

        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="execute">The execute.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        /// <exception cref="System.ArgumentNullException">execute</exception>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException("execute");
            _canExecute = canExecute;
        }


        /// <summary>
        /// 명령을 실행해야 하는지 여부에 영향을 주는 변경이 발생할 때 발생합니다.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// 명령을 현재 상태에서 실행할 수 있는지를 결정하는 메서드를 정의합니다.
        /// </summary>
        /// <param name="parameter">명령에 사용된 데이터입니다.
        /// 명령에서 데이터를 전달할 필요가 없으면 이 개체를 <see langword="null" />로 설정할 수 있습니다.</param>
        /// <returns>
        /// 이 명령을 실행할 수 있으면 <see langword="true" />이고, 그러지 않으면 <see langword="false" />입니다.
        /// </returns>
        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        /// <summary>
        /// 명령이 호출될 때 호출될 메서드를 정의합니다.
        /// </summary>
        /// <param name="parameter">명령에 사용된 데이터입니다.
        /// 명령에서 데이터를 전달할 필요가 없으면 이 개체를 <see langword="null" />로 설정할 수 있습니다.</param>
        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
