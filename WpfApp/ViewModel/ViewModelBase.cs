﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.ComponentModel;

namespace WpfApp.ViewModel
{
    // 이쪽 코드는 MSDN에서 올려준 예제코드를 받아서 넣어두었는데
    // RaisePropertyChanged, PropertyChangedEventHandler 가 왜 필요한지는 알겠는데
    // 그 아래에 있는 코드들은 앱닫기 버튼 누를때,
    // 그에 대한 처리를 해주는 것 같은데, 설명이 없어서 맞는지 모르겠다.
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class ViewModelBase : INotifyPropertyChanged
    {
        //basic MyColorBase
        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <param name="prop">The property.</param>
        internal void RaisePropertyChanged(string prop) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        /// <summary>
        /// 속성 값이 변경될 때 발생합니다.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        //Extra Stuff, shows why a base ViewModel is useful
        /// <summary>
        /// The close window flag
        /// </summary>
        bool? _CloseWindowFlag;
        /// <summary>
        /// Gets or sets the close window flag.
        /// </summary>
        /// <value>
        /// The close window flag.
        /// </value>
        public bool? CloseWindowFlag
        {
            get { return _CloseWindowFlag; }
            set
            {
                _CloseWindowFlag = value;
                RaisePropertyChanged("CloseWindowFlag");
            }
        }

        /// <summary>
        /// Closes the window.
        /// </summary>
        /// <param name="result">The result.</param>
        public virtual void CloseWindow(bool? result = true)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                CloseWindowFlag = CloseWindowFlag == null
                    ? true
                    : !CloseWindowFlag;
            }));
        }
    }
}
