﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows.Media;
using WpfApp.Helper;
using WpfApp.Model;

namespace WpfApp.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="WpfApp.ViewModel.ViewModelBase" />
    public class ViewModelMain : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the sample colors.
        /// </summary>
        /// <value>
        /// The sample colors.
        /// </value>
        public ObservableCollection<MyColor> SampleColors { get; set; } = new ObservableCollection<MyColor> { };
        /// <summary>
        /// Gets or sets my colors.
        /// </summary>
        /// <value>
        /// My colors.
        /// </value>
        public ObservableCollection<MyColor> MyColors { get; set; } = new ObservableCollection<MyColor> { };

        /// <summary>
        /// The selected color
        /// </summary>
        object _SelectedColor;
        /// <summary>
        /// Gets or sets the color of the selected.
        /// </summary>
        /// <value>
        /// The color of the selected.
        /// </value>
        public object SelectedColor
        {
            get
            {
                return _SelectedColor;
            }
            set
            {
                if (_SelectedColor != value)
                {
                    _SelectedColor = value;
                    RaisePropertyChanged("SelectedColor");
                }
            }
        }
        /// <summary>
        /// The selected hexadecimal number
        /// </summary>
        object _SelectedHexNumber;
        /// <summary>
        /// Gets or sets the selected hexadecimal number.
        /// </summary>
        /// <value>
        /// The selected hexadecimal number.
        /// </value>
        public object SelectedHexNumber
        {
            get
            {
                return _SelectedHexNumber;
            }
            set
            {
                if (_SelectedHexNumber != value)
                {
                    _SelectedHexNumber = value;
                    RaisePropertyChanged("SelectedHexNumber");
                }
            }
        }

        /// <summary>
        /// The selected hexadecimal color
        /// </summary>
        object _SelectedHexColor;
        /// <summary>
        /// Gets or sets the color of the selected hexadecimal.
        /// </summary>
        /// <value>
        /// The color of the selected hexadecimal.
        /// </value>
        public object SelectedHexColor
        {
            get
            {
                return _SelectedHexColor;
            }
            set
            {
                if (_SelectedHexColor != value)
                {
                    _SelectedHexColor = value;
                    RaisePropertyChanged("SelectedHexColor");
                }
            }
        }

        /// <summary>
        /// Gets or sets the add user command.
        /// </summary>
        /// <value>
        /// The add user command.
        /// </value>
        public RelayCommand AddUserCommand { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModelMain" /> class.
        /// </summary>
        public ViewModelMain()
        {
            //색상에 대한 속성을 받아 열거형 IEnumerable 변수로 저장하고
            IEnumerable<PropertyInfo> Properties =
            typeof(Colors).GetTypeInfo().DeclaredProperties;

            //가지고 있는 색상을 리스트에 저장
            foreach (PropertyInfo property in Properties)
            {
                Color clr = (Color)property.GetValue(null);

                MyColor myColor = new MyColor()
                {
                    ColorName = (property as PropertyInfo).Name,
                    HexColor = clr.ToString(),
                    HexNumber = clr.ToString()
                };

                SampleColors.Add(myColor);
            }
            AddUserCommand = new RelayCommand(AddUser);
        }

        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        void AddUser(object parameter)
        {
            if (parameter == null) return;
            MyColors.Add(new MyColor(parameter));
        }
    }
}
