﻿using System.Windows;

namespace WpfApp.View
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// 개인적으로 만들어본 프로그램을 MVVM패턴으로 수정해보았다.
    /// 그런데... 이게 왜 만들어졌고 이론이나 여러가지 찾아보니 좋다는건 알겠는데,
    /// 실제로 팀내 협업을 통해서 이게 왜 필요한지 경험해보질 못해서
    /// 정말 좋긴한건지 모르겠다. 
    /// 코드짜기만 어렵고..
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
