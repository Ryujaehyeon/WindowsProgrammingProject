﻿using System.ComponentModel;

namespace WpfApp.Model
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class MyColor : INotifyPropertyChanged
    {
        /// <summary>
        /// The color name
        /// </summary>
        object _ColorName;
        /// <summary>
        /// Gets or sets the name of the color.
        /// </summary>
        /// <value>
        /// The name of the color.
        /// </value>
        public object ColorName
        {
            get { return _ColorName; }
            set
            {
                if (_ColorName != value)
                {
                    _ColorName = value;
                    RaisePropertyChanged("ColorName");
                }
            }
        }

        /// <summary>
        /// The hexadecimal number
        /// </summary>
        object _HexNumber;
        /// <summary>
        /// Gets or sets the hexadecimal number.
        /// </summary>
        /// <value>
        /// The hexadecimal number.
        /// </value>
        public object HexNumber
        {
            get
            {
                return _HexNumber;
            }
            set
            {
                if (_HexNumber != value)
                {
                    _HexNumber = value;
                    RaisePropertyChanged("HexNumber");
                }
            }
        }

        /// <summary>
        /// The hexadecimal color
        /// </summary>
        object _HexColor;
        /// <summary>
        /// Gets or sets the color of the hexadecimal.
        /// </summary>
        /// <value>
        /// The color of the hexadecimal.
        /// </value>
        public object HexColor
        {
            get
            {
                return _HexColor;
            }
            set
            {
                if (_HexColor != value)
                {
                    _HexColor = value;
                    RaisePropertyChanged("HexColor");
                }
            }
        }

        //public override string ToString()
        //{
        //    return ColorName;
        //}
        /// <summary>
        /// Initializes a new instance of the <see cref="MyColor"/> class.
        /// </summary>
        public MyColor() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="MyColor"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        public MyColor(object property)
        {
            ColorName = (property as MyColor).ColorName;
            HexNumber = (property as MyColor).HexNumber;
            HexColor = (property as MyColor).HexColor;
        }
        //public MyColor(string _colorname, string _HexNumber)
        //{
        //    ColorName = _colorname;
        //    HexNumber = _HexNumber;

        //    //hex to color
        //    //Color color = (Color)ColorConverter.ConvertFromString(selectColor.HexNumber);

        //    //color to hex
        //    //string hexcolor = color.ToString();

        //    HexColor = (Color)ColorConverter.ConvertFromString(_HexNumber);
        //}

        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <param name="prop">The property.</param>
        void RaisePropertyChanged(string prop) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        /// <summary>
        /// 속성 값이 변경될 때 발생합니다.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}

