﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLLCalculate.CCalculator
{
    /// <summary>
    /// 폴란드 접두사 표기법 계산기 클래스.
    /// 폴란드 표기법(전위식 또는 후위식)으로 처리 후, 연산하고 그 결과값을 가지고 반환하는 클래스.
    /// </summary>
    public class CCalculator
    {
        /// <summary>
        /// 피연산자를 좌항인지 우항인지 결정해주기 위한 열거형
        /// </summary>
        enum Operand { LEFT = 0, RIGHT = 1 };

        /// <summary>
        /// 표기법에 대한 Flag 필드
        /// </summary>
        public bool FixFlag { get; set; } = false;

        /// <summary>
        /// 연산자를 정의하고 판별하는 필드
        /// </summary>
        readonly List<string> Operator = new List<string> { "+", "-", "*", "/", "(", ")", "!" };

        /// <summary>
        /// 연산시에 연산자를 담아둘 스택
        /// </summary>
        Stack<string> StackOperator = new Stack<string>();

        /// <summary>
        /// 연산자 우선순위를 결정
        /// </summary>
        private readonly Dictionary<string, int> Priority = new Dictionary<string, int>
        {
            ["*"] = 2,
            ["/"] = 2,
            ["+"] = 1,
            ["-"] = 1,
            ["("] = 0,
            [")"] = 0,
        };

        /// <summary>
        ///   <see cref="CCalculator" /> 클래스의 인스턴스를 초기화하는 생성자.
        /// </summary>
        public CCalculator()
        {
        }

        /// <summary>
        /// 문자열 분할 메서드
        /// </summary>
        /// <param name="inputvalue">입력받은 문자열</param>
        /// <returns>
        /// 분할한 문자들이 담긴 리스트를 반환
        /// </returns>
        List<string> StringSplit(string inputvalue)
        {
            inputvalue.Replace(" ", "");

            //단위별로 쪼갠 문자열을 담는 리스트 
            // 10-(2*5)/2 =>
            // "10", "-", "(", "2", "*", "5", ")", "/", "2" 
            List<string> tokens = new List<string>();

            // 10진수를 담는다.
            StringBuilder decimalBuffer = new StringBuilder();

            // 스트링을 담는다.
            StringBuilder stringBuffer = new StringBuilder();

            // char로 분할
            char[] arrayToken = inputvalue.ToCharArray();

            // 문자열에서 분할된 문자들을 단위별로 묶고 구분한다.
            foreach (char item in arrayToken)
            {
                // Item은 숫자인가
                if (Char.IsNumber(item))
                {
                    // stringBuffer에 무언가 들어있으면
                    if (stringBuffer.Length > 0)
                    {
                        // stringbuffer에 있는 값이 Operator에 있는지 확인
                        if (Operator.Contains(stringBuffer.ToString()))
                        {
                            // 있으면 tokens에 추가
                            tokens.Add(stringBuffer.ToString());
                            // 그리고 비운다.
                            stringBuffer.Clear();
                        }
                    }
                    // 넣는다.
                    decimalBuffer.Append(item);
                }
                // 숫자가 아니라 문자면
                else
                {
                    // decimalBuffer에 무언가 들어있으면
                    if (decimalBuffer.Length > 0)
                    {
                        // 일단 넣는다
                        tokens.Add(decimalBuffer.ToString());
                        decimalBuffer.Clear();
                    }

                    // 문자인데 오퍼레이터일때 - 사칙 연산 및 기호
                    if (Operator.Contains(item.ToString()))
                    {
                        tokens.Add(item.ToString());
                        continue;
                    }
                    else
                    {
                        // TODO : 나중에 sin cos tan 및 문자열로 된 oper를 자를때 이곳에 추가.
                    }
                }
            }

            // stringBuffer에 무언가 들어있으면
            if (stringBuffer.Length > 0)
            {
                // stringbuffer에 있는 값이 Operator에 있는지 확인
                if (Operator.Contains(stringBuffer.ToString()))
                {
                    // 있으면 tokens에 추가
                    tokens.Add(stringBuffer.ToString());
                    // 그리고 비운다.
                    stringBuffer.Clear();
                }
            }

            // decimalBuffer에 무언가 들어있으면
            if (decimalBuffer.Length > 0)
            {
                // 일단 넣는다
                tokens.Add(decimalBuffer.ToString());
                decimalBuffer.Clear();
            }

            // 분할한 문자열을 반환
            return tokens;
        }

        /// <summary>
        /// 매개 변수로 문자열리스트를 받고 <c>bool</c>형 <c>isfix</c>로 가공 방식을 정한 후 해당식으로 변환
        /// </summary>
        /// <param name="tokens">입력받은 문자열 리스트</param>
        /// 
        /// <returns>
        /// 후위식으로 처리된 문자열리스트를 리턴
        /// </returns>
        public List<string> ConvertToFix(List<string> tokens)
        {
            List<string> resulttokens = new List<string>();
            string[] bracket = new string[] { "(", ")" };

            foreach (string item in tokens)
            {
                // 토큰별로 오퍼레이터와 비교한다
                // 연산자일때
                if (Operator.Contains(item))
                {
                    // 왼쪽 괄호면
                    if (item == bracket[(int)Operand.LEFT])
                    {
                        // 스택에 추가한다.
                        StackOperator.Push(item);
                    }
                    // 오른쪽 괄호면
                    else if (item == bracket[(int)Operand.RIGHT])
                    {
                        // 스택의 맨위 항목이 왼쪽괄호["("]가 아니면 반복한다.
                        while (StackOperator.Peek() != bracket[(int)Operand.LEFT])
                        {
                            // 왼쪽 괄호가 아니면 넣고
                            resulttokens.Add(StackOperator.Pop());

                            // 다음 최상위 요소가 왼쪽괄호인지 체크
                            if (StackOperator.Peek() == bracket[(int)Operand.LEFT])
                            {
                                // 왼쪽괄호면 버린다.
                                StackOperator.Pop();
                                //반복문을 벗어난다.
                                break;
                            }
                        }
                    }
                    else
                    {
                        // 스택이 비어있지 않다면
                        if (StackOperator.Count != 0)
                        {
                            // 현재 아이템의 우선순위가 스택의 최상단 요소보다 우선순위가 
                            // 클때까지 
                            while (Priority[StackOperator.Peek()] >= Priority[item])
                            {
                                // 스택의 요소를 꺼내 출력에 넣고
                                resulttokens.Add(StackOperator.Pop());

                                // 스택이 0이되면 반복문 탈출
                                if (StackOperator.Count == 0)
                                {
                                    break;
                                }
                            }

                            // 스택에 넣는다
                            StackOperator.Push(item);
                        }
                        // 비어있다면
                        else
                        {
                            //스택에 넣는다
                            StackOperator.Push(item);
                        }
                    }
                }
                // 피연산자 일때
                else
                {
                    // 출력에 추가
                    resulttokens.Add(item);
                }
            }

            // 스택에 남아있는 연산자를 출력에 추가해준다.
            while (StackOperator.Count != 0)
            {
                string elementitem = StackOperator.Pop();
                if (elementitem != bracket[(int)Operand.LEFT] &&
                    elementitem != bracket[(int)Operand.RIGHT])
                {
                    resulttokens.Add(elementitem);
                }
            }

            return resulttokens;
        }

        /// <summary>
        /// 식과 연산방법에 대한 정보를 받아서 처리하고 계산하는 메서드
        /// </summary>
        /// <param name="expression">식에 대한 문자열 매개변수</param>
        /// <param name="fixflag">연산방법에 대한 정보 <c>true</c> 는 전위식, <c>false</c> 는 후위식.</param>
        /// <param name="fixexp">The fixexp.</param>
        /// <param name="result">The result.</param>
        /// <returns>
        /// 계산한 값을 리턴한다.
        /// </returns>
        public string Calculate(string expression, bool fixflag, ref string fixexp, ref string result)
        {
            // 문자열(입력된 식)을 분할해서 담는 리스트
            List<string> expressionsplit = StringSplit(expression);

            // 계산할 리스트
            List<string> calculationlist = new List<string>();

            // 피연산자 및 계산된 값을 담는 스택
            Stack<string> calStack = new Stack<string>();

            // 피연산자를 담을 로컬변수
            // 우항부터 기술한 이유는 스택에서 꺼낼때 최상단의 데이터를 꺼내기 때문에
            // 꺼낸 요소는 우항부터 대입해줄 필요가 있기 때문[후위 연산 기준으로 지역변수를 정했기 때문에 우항부터]
            Dictionary<Operand, decimal> binaryOperand = new Dictionary<Operand, decimal>
            {
                //우항
                [Operand.RIGHT] = 0,
                //좌항
                [Operand.LEFT] = 0
            };

            // 사전형식 binaryOperand의 Key만 리스트로 받는다.
            List<Operand> operandlist = binaryOperand.Keys.ToList();

            // 연산자 표기법에 따라 가공
            calculationlist = ConvertToFix(expressionsplit);

            if (fixflag)
            {
                calculationlist.Reverse();

                for (int i = calculationlist.Count-1; i >= 0; i--)
                {

                    // 요소가 연산자면
                    if (Operator.Contains(calculationlist[i]))
                    {
                        // 스택에서 좌항, 우항에 하나씩 꺼내서 담는다.
                        // 사전을 ToList()를 사용하여 리스트로 변환한 이유는 사전타입은 value가 바뀌면 에러를 발생시키기때문에 변환한것.
                        // foreach이기 때문에 binaryOperand의 요소가 최대 2개이기때문에 최대 2번의 싸이클만 돌아서 사전의 요소를 덮어쓴다.
                        foreach (Operand key in operandlist)
                        {
                            // 리스트로 변환했어도 순서는 [우항, 좌항]
                            if (calStack.Count == 0)
                                return "잘못된 입력값입니다.";
                            else
                                binaryOperand[key] = decimal.Parse(calStack.Pop());
                        }

                        //요소가 연산자이니, 피연산자를 계산할 메서드를 찾아가서 계산 후, calstack에 담는다.
                        switch (calculationlist[i])
                        {
                            case "+":
                                {
                                    // + 함수
                                    calStack.Push((binaryOperand[Operand.LEFT] + binaryOperand[Operand.RIGHT]).ToString());

                                    break;
                                }
                            case "-":
                                {
                                    // - 함수
                                    calStack.Push((binaryOperand[Operand.LEFT] - binaryOperand[Operand.RIGHT]).ToString());

                                    break;
                                }
                            case "*":
                                {
                                    // * 함수
                                    calStack.Push((binaryOperand[Operand.LEFT] * binaryOperand[Operand.RIGHT]).ToString());

                                    break;
                                }
                            case "/":
                                {
                                    // / 함수
                                    calStack.Push(decimal.Round((binaryOperand[Operand.LEFT] / binaryOperand[Operand.RIGHT]),5).ToString());

                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    else
                    {
                        // 피연산자면 스택에 push하고
                        calStack.Push(calculationlist[i]);
                    }
                }
            }
            else
            {
                // 계산해야 할 식(calculationlist)이 담긴 리스트를 순회하며,
                // 요소가 피연산자면, calStack에 Push. 
                // 요소가 연산자면,   calStack에서 피연산자를 하나씩 Pop.
                // binaryOperand(우항, 좌항)에 순서대로 넣고 계산 후 
                // 결과를 Push
                foreach (string element in calculationlist)
                {
                    // 요소가 연산자면
                    if (Operator.Contains(element))
                    {
                        // 스택에서 좌항, 우항에 하나씩 꺼내서 담는다.
                        // 사전을 ToList()를 사용하여 리스트로 변환한 이유는 사전타입은 value가 바뀌면 에러를 발생시키기때문에 변환한것.
                        // foreach이기 때문에 binaryOperand의 요소가 최대 2개이기때문에 최대 2번의 싸이클만 돌아서 사전의 요소를 덮어쓴다.
                        foreach (Operand key in operandlist)
                        {
                            // 리스트로 변환했어도 순서는 [우항, 좌항]
                            if (calStack.Count == 0)
                                return "잘못된 입력값입니다.";
                            else
                                binaryOperand[key] = decimal.Parse(calStack.Pop());
                        }

                        //요소가 연산자이니, 피연산자를 계산할 메서드를 찾아가서 계산 후, calstack에 담는다.
                        switch (element)
                        {
                            case "+":
                                {
                                    // + 함수
                                    calStack.Push((binaryOperand[Operand.LEFT] + binaryOperand[Operand.RIGHT]).ToString());
                                    break;
                                }
                            case "-":
                                {
                                    // - 함수
                                    calStack.Push((binaryOperand[Operand.LEFT] - binaryOperand[Operand.RIGHT]).ToString());

                                    break;
                                }
                            case "*":
                                {
                                    // * 함수
                                    calStack.Push((binaryOperand[Operand.LEFT] * binaryOperand[Operand.RIGHT]).ToString());

                                    break;
                                }
                            case "/":
                                {
                                    // / 함수
                                    calStack.Push(decimal.Round(binaryOperand[Operand.LEFT] / binaryOperand[Operand.RIGHT],
                                        Convert.ToString(binaryOperand[Operand.LEFT] / binaryOperand[Operand.RIGHT]).Length / 2).ToString());
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    else
                    {
                        // 피연산자면 스택에 push하고
                        calStack.Push(element);
                    }
                }
            }

            // 결과값을 리턴
            if (calStack.Count != 0)
            {
                fixexp = string.Join("", calculationlist.ToArray());
                return result = calStack.Pop();
            }
            else
                return "잘못된 입력 값입니다.";
        }

        /// <summary>
        /// 인스턴스 내의 필드와 속성을 초기화시킨다.
        /// </summary>
        public void Clear()
        {
            StackOperator.Clear();
        }
    }
}
