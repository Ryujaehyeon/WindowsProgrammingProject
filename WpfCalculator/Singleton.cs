﻿using System;

namespace Singleton.Class
{
    // MSDN에서 참조한 Lazy Singleton
    // 일단 Lazy Singleton 자체는 MSDN에서 Sample로 알려준 MultiThread-Safe Singleton코드이고 
    // 그 코드를 기반으로 어디서든 사용가능하게 일반화해본 코드
    // 이렇게 만들어 사용해도 되는지 모르겠다.
    // 믿어도 되나 이거...?

    /// <summary>
    /// Generic Lazy Singleton이다.
    /// </summary>
    /// <typeparam name="T">Typename T</typeparam>
    public sealed class Singleton<T> where T : class, new()
    {
        /// <summary>
        ///   <seealso cref="Singleton{T}" />의 생성자
        /// </summary>
        Singleton() { }

        /// <summary>
        /// 필드 초기화를 <see cref="Lazy{T}" />로 인스턴스 생성 지점을 늦춘다. <see cref="lazyInstance" />의 <see cref="Instance" /> 호출시 생성.
        /// </summary>
        private static readonly Lazy<T> lazyInstance = new Lazy<T>(() => new T());

        /// <summary>
        ///   <see cref="Singleton{T}" />의 인스턴스가 생성된 필드인 <see cref="lazyInstance" />를 호출하는 정적 속성(<c>public static T Instance</c>).
        /// </summary>
        /// <value>
        /// 인스턴스
        /// </value>
        public static T Instance
        {
            get
            {
                return lazyInstance.Value;
            }
        }
    }
}
