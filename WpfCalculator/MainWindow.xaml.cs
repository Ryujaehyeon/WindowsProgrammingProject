﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Singleton.Class;
using DLLCalculate.CCalculator;

namespace WpfCalculator
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class MainWindow : Window
    {
        /// <summary>
        /// 전위식으로 계산할지 후위식으로 계산할지에 대한 Flag,
        /// True는 전위식, False는 후위식
        /// </summary>
        bool FixFlag = false;

        /// <summary>
        ///   <see cref="MainWindow" /> 클래스의 새 인스턴스를 초기화.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Btn_Num 컨트롤의 Click 이벤트를 처리합니다.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> 인스턴스에 포함된 이벤트 데이터 </param>
        private void Btn_Num_Click(object sender, RoutedEventArgs e)
        {
            txt_Expression.Text += (sender as Button).Content;
        }

        /// <summary>
        /// Btn_Oper 컨트롤의 Click 이벤트를 처리합니다.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> 인스턴스에 포함된 이벤트 데이터 </param>
        private void Btn_Oper_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            txt_Expression.Text += btn.Content.ToString();
        }

        /// <summary>
        /// Btn_Clear  컨트롤의 Click 이벤트를 처리합니다.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> 인스턴스에 포함된 이벤트 데이터 </param>
        private void Btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            txt_Expression.Text = "";
            txt_Result.Text = "";
            Singleton<CCalculator>.Instance.Clear();
        }

        /// <summary>
        /// Btn_Equals 컨트롤의 Click 이벤트를 처리합니다.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> 인스턴스에 포함된 이벤트 데이터</param>
        private void Btn_Equals_Click(object sender, RoutedEventArgs e)
        {
            string fixexp = "";
            string result = "";
            //싱글톤으로 생성된 계산기 인스턴스에 입력값과 설정값을 주고 계산기 인스턴스의 Expression메서드가 계산 후 string 반환
            Singleton<CCalculator>.Instance.Calculate(txt_Expression.Text, FixFlag, ref fixexp,  ref result);

            txt_fixExpression.Text = fixexp;
            txt_Result.Text = result;
        }

        /// <summary>
        /// Btn_Conversion 컨트롤의 Click 이벤트를 처리합니다.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> 인스턴스에 포함된 이벤트 데이터</param>
        private void Btn_Conversion_Click(object sender, RoutedEventArgs e)
        {
            if (txt_Expression.Text.First().Equals('-'))
                txt_Expression.Text = txt_Expression.Text.Remove(0,1);
            else
                txt_Expression.Text = "-" + txt_Expression.Text ;
        }

        /// <summary>
        /// Btn_Period 컨트롤의 Click 이벤트를 처리합니다.
        /// </summary> 
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> 인스턴스에 포함된 이벤트 데이터 </param>
        private void Btn_Period_Click(object sender, RoutedEventArgs e)
        {
            if (txt_Expression.Text.Last().Equals('.'))
                txt_Expression.Text = txt_Expression.Text.Remove(txt_Expression.Text.Length-1);
            else
                txt_Expression.Text = txt_Expression.Text + ".";
        }

        /// <summary>
        /// AnyKey 이벤트로 KeyDown 이벤트를 처리합니다.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs" /> 인스턴스에 포함된 이벤트 데이터 </param>
        private void AnyKey_Down(object sender, KeyEventArgs e)
        {
            Key key = e.Key;
            switch (key)
            {
                case Key.Tab:
                    {
                        Btn_Conversion_Click(sender, e);
                    }
                    break;
                case Key.Escape:
                    {
                        Btn_Clear_Click(sender, e);
                    }
                    break;
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                case Key.NumPad0:
                case Key.NumPad1:
                case Key.NumPad2:
                case Key.NumPad3:
                case Key.NumPad4:
                case Key.NumPad5:
                case Key.NumPad6:
                case Key.NumPad7:
                case Key.NumPad8:
                case Key.NumPad9:
                    {
                        Btn_Num_Click(sender, e);
                    }
                    break;
                case Key.Multiply:
                case Key.Add:
                case Key.Subtract:
                case Key.Divide:
                    {
                        Btn_Oper_Click(sender, e);
                    }
                    break;
                case Key.Enter:
                case Key.OemPlus:
                    {
                        Btn_Equals_Click(sender, e);
                    }
                    break;
                case Key.OemPeriod:
                    {
                        Btn_Period_Click(sender, e);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
