﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;
namespace WpfCharClientProgramming
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        IPEndPoint ServerIP = new IPEndPoint(IPAddress.Any, 10001);
        IPEndPoint MyIP = new IPEndPoint(IPAddress.Any, 10001);
        public MainWindow()
        {
            InitializeComponent();
            masktxtbox_ServerIP.Text = ServerIP.Address.ToString();
            txtbox_ServerPort.Text = ServerIP.Port.ToString();
        }

        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char c = Convert.ToChar(e.Text);
            if (Char.IsNumber(c)||c.Equals('.'))
                e.Handled = false;
            else
                e.Handled = true;

            base.OnPreviewTextInput(e);
        }
    }
}
